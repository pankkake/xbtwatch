#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

import json
import os
import socket
import sys
from decimal import Decimal

from xbtwatch import Watcher

TARGET = os.getenv('IRK_TARGET', 'irc://chat.freenode.net/altcoin')

LEFT = 'ATC'
RIGHT = 'sBTC'
VOL = 'BTC'


class MyWatcher(Watcher):
    SLEEP = 300

    def log(self, error):
        print(error, file=sys.stderr)


def irk(trade):
    left = Decimal(trade['vol'])
    right = Decimal(trade['price'])
    vol = left * right
    bs = trade['type'][0].upper()
    message = '{:12.2f} {} @ {:4.0f} {} = {:4.2f} {} [{}]' \
        .format(left, LEFT, right * 10**8, RIGHT, vol, VOL, bs)

    data = {'to': TARGET, 'privmsg': message}
    s = socket.create_connection(('localhost', 6659))
    s.sendall(json.dumps(data))

w = MyWatcher('atcbtc')
w.watch(irk)
