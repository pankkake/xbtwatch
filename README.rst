x-bt.com watcher
================

**xbtwatch** is a tool and library able to stream new
`X-BT <https://x-bt.com/ref-nw1dkato4h3v2qr6jgnkb90ngz0z2iip>`_ trades.
It is based on `bcwatch <https://bitbucket.org/pankkake/bcwatch>`_,
but is crappier.


Requirements
------------

- Python 2.6 or higher
- requests 1.2 or higher


Credits
-------

If you like this script, please tip 1Md5kvYempaW3NkYxdYbFezL6mKg7QwERe (BTC)
or 1PonZioHc2gKHhSx3mbXD2tfahJh2vefSi (ATC).
