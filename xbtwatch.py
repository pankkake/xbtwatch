#!/usr/bin/env python
from __future__ import absolute_import, print_function, unicode_literals

import time

import requests


class Watcher(object):
    URL = 'https://x-bt.com/api/{market}/get_trades'
    SLEEP = 600

    def __init__(self, market, session=None, tradeid=None):
        self.market = market
        self.tradeid = tradeid
        if not session:
            session = requests.Session()
            session.timeout = 120
            session.verify = True
        self.session = session

    def sleep(self):
        time.sleep(self.SLEEP)

    def watch(self, callback, assume_new=False):
        while True:
            self.update(callback, assume_new)
            self.sleep()

    def update(self, callback, assume_new=False):
        for newtrade in self.fetch(seen=not assume_new):
            callback(newtrade)

    def log(self, error):
        pass

    def fetch(self, seen=True):
        try:
            r = self.session.get(self.URL.format(market=self.market), stream=False)
            r.raise_for_status()
            trades = r.json()['ok']
        except (ValueError, KeyError, requests.exceptions.RequestException) as error:
            self.log(error)
            return
        # if has been run once already; disable assume_new
        if self.tradeid:
            seen = True
        for trade in reversed(trades):
            tradeid = trade['id']
            if not seen:
                yield trade
            elif tradeid == self.tradeid:
                # everything after last known uuid is unseen
                seen = False
        # if it happens, it means we missed trades, and that case
        # isn't handled yet
        assert seen is False or self.tradeid is None
        self.tradeid = tradeid


if __name__ == '__main__':
    w = Watcher('atcbtc')
    w.watch(print, True)
